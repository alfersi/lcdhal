#ifndef PINOUT_H
#define PINOUT_H

#define LCD_RS_DDR DDRF
#define LCD_RW_DDR DDRF
#define LCD_E_DDR DDRF
#define LCD_RS_PORT PORTF
#define LCD_RS_BIT 0
#define LCD_RW_PORT PORTF
#define LCD_RW_BIT 1
#define LCD_E_PORT PORTF
#define LCD_E_BIT 2
#define LCD_DATA_PORT PORTC
#define LCD_DATA_DDR DDRC

#endif