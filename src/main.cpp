#include <Arduino.h>
#include "LCDHAL.h"
#include "PINOUT.h"

LCDHAL ReespiratorLCD;

void setup() 
{ 
  char TestString0[]="Test linea 0 0123456";
  char TestString1[]="Test linea 1 1234567";
  char TestString2[]="Test linea 2 2345678";
  char TestString3[]="Test linea 3 3456789";

  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  
  Serial.begin(115200);

  ReespiratorLCD.LCDPortInit();
  ReespiratorLCD.LCDInit();

  ReespiratorLCD.LCDGoto(0,0);
  ReespiratorLCD.LCDWrite(TestString0,20);
  ReespiratorLCD.LCDGoto(0,1);
  ReespiratorLCD.LCDWrite(TestString1,20);
  ReespiratorLCD.LCDGoto(0,2);
  ReespiratorLCD.LCDWrite(TestString2,20);
  ReespiratorLCD.LCDGoto(0,3);
  ReespiratorLCD.LCDWrite(TestString3,20);
}

void loop() 
{
  if(Serial.available()) ReespiratorLCD.LCDPutchar(Serial.read());

  ReespiratorLCD.Update();

  delay(1);
}